Source: hylafax
Section: comm
Priority: extra
Maintainer: Giuseppe Sacco <eppesuig@debian.org>
Uploaders: Joachim Wiedorn <joodebian@joonet.de>
Build-Depends: debhelper (>= 11),
 autotools-dev, libjbig-dev,
 libtiff5-dev (>= 4.0.1-3),
 libtiff-tools (>= 4.0.1-3),
 zlib1g-dev (>= 0.95), ed,
 libpam-dev | libpam0g-dev,
 gsfonts, ghostscript,
 dpkg (>= 1.14.6),
 init-system-helpers (>= 1.51) 
Standards-Version: 4.5.0
Homepage: http://www.hylafax.org
Vcs-git: git://git.hylafax.org/HylaFAX

Package: hylafax-server
Architecture: any
Section: comm
Depends: ${shlibs:Depends}, ${misc:Depends},
 hylafax-client (= ${binary:Version}),
 libtiff-tools (>= 4.0.1-3),
 bsd-mailx | mailx, psmisc,
 sed (>= 4.1.2), ghostscript,
 adduser, lsb-base (>= 3.0-6),
 exim4-daemon-light | mail-transport-agent,
 systemd (>> 235-3)
Suggests: mgetty, psrip
Conflicts: mgetty-fax, capi4hylafax (<< 1:01.02.03-4)
Description: Flexible client/server fax software - server daemons
 This package support the sending and receiving of facsimiles, the polled
 retrieval of facsimiles and the send of alphanumeric pages.
 .
 The host running the server must have either a Class 1, Class 2, or a
 Class 2.0 fax modem attached to one of its serial ports. End-user
 applications to manage the transmission of documents via facsimile are
 provided separately by the hylafax-client package.

Package: hylafax-client
Architecture: any
Section: comm
Depends: ${shlibs:Depends}, ${misc:Depends},
 enscript | libgnomeprint-data,
 ucf, gsfonts, ghostscript
Pre-Depends: libpaper-utils
Suggests: mgetty-viewfax
Recommends: netpbm, transfig
Conflicts: mgetty-fax
Description: Flexible client/server fax software - client utilities
 The HylaFAX client software communicates with a HylaFAX server via TCP/IP.
 .
 HylaFAX support the sending and receiving of facsimiles, the polled
 retrieval of facsimiles and the send of alphanumeric pages.

Package: hylafax-server-dbg
Section: debug
Architecture: any
Depends: hylafax-server (= ${binary:Version}), ${misc:Depends}
Description: Debug symbols for the hylafax server
 This package support the sending and receiving of facsimiles, the polled
 retrieval of facsimiles and the send of alphanumeric pages.
 .
 The host running the server must have either a Class 1, Class 2, or a
 Class 2.0 fax modem attached to one of its serial ports. End-user
 applications to manage the transmission of documents via facsimile are
 provided separately by the hylafax-client package.
 .
 This package only contains debugging symbols.

Package: hylafax-client-dbg
Architecture: any
Section: debug
Depends: hylafax-client (= ${binary:Version}), ${misc:Depends}
Description: Flexible client/server fax software - client utilities
 The HylaFAX client software communicates with a HylaFAX server via TCP/IP.
 .
 HylaFAX support the sending and receiving of facsimiles, the polled
 retrieval of facsimiles and the send of alphanumeric pages.
 .
 This package only contains debugging symbols.
